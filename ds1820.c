/************************************************************************************************
 * 	Projekt:	Funktionen für DS18x20 1-Wire Thermometer
 * 	Autor:		Jan-Ole Spahn
 * 	Beschreibung:	Devicespezifische Routinen für 1-Wire Digitalthermometer
 * 	Version:	2.0, 02.12.2013 Gemeinsame Version für MSP430 und AVR, wählbar über
 *				        	 #define AVR oder #define __msp430 in owi.h
 * 			1.1, 16.11.2013 Portiert für MSP430, Unterstützung für DS18B20 und DS1822
 * 					hinzugefügt
 * 			1.0, 15.02.2012 Nur der DS18S20 wird momentan untesrtützt
 ************************************************************************************************/

#ifdef AVR
  #include <avr/interrupt.h>
#endif
#include <stdlib.h>
#include "ds1820.h"
#include "owi.h"
#include "owicrc.h"

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Temperaturmessung durchführen lassem.
 * Parameter: romcode oder 0 für alle
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_StartConversion(uint8_t *romcode, uint8_t pullstrong) 
{
	uint8_t i;
	__istate_t istate;  
	if(OWI_reset()==OWI_RST_OK){
		istate = __get_interrupt_state();
		__disable_interrupt();	/* ATOMIC !!! */
		/* Wenn ein Romcode übergeben wurde diesen anwehlen, sonst SKIP ROM senden */
		if (romcode) {
			OWI_WriteByte(OWI_MATCH_ROM);
			for (i=0; i<8; i++) {
				OWI_WriteByte(*romcode++);
			}
		} else {
			OWI_WriteByte(OWI_SKIP_ROM);
		}
		/* CONVERT T senden und ggf. Strong Pull Up aktivieren. */
		OWI_WriteByte(OWI_CONVERT_T);
		if (pullstrong == OWI_PULL_STRONG) {
			OWI_pull_high();
		}
		__set_interrupt_state(istate); /* ATOMIC END */
	}	
}

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Register TH und TL ins EPROM schreiben lassen
 * Parameter: romcode oder 0 für alle
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_CopyScratchpad(uint8_t *romcode, uint8_t pullstrong) 
{
	uint8_t i;
	__istate_t istate;  
	if(OWI_reset()==OWI_RST_OK){
		istate = __get_interrupt_state();
		__disable_interrupt();	/* ATOMIC !!! */
		/* Wenn ein Romcode übergeben wurde diesen anwehlen, sonst SKIP ROM senden */
		if (romcode) {
			OWI_WriteByte(OWI_MATCH_ROM);
			for (i=0; i<8; i++) {
				OWI_WriteByte(*romcode++);
			}
		} else {
			OWI_WriteByte(OWI_SKIP_ROM);
		}
		OWI_WriteByte(OWI_COPY_SCRACTHPAD);
		if (pullstrong == OWI_PULL_STRONG) {
			OWI_pull_high();
		}
		__set_interrupt_state(istate); /* ATOMIC END */
	}
}

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Register TH und TL aus dem EPROM laden lassen
 * Parameter: romcode oder 0 für alle
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_RecallEeprom(uint8_t *romcode, uint8_t pullstrong) 
{
	uint8_t i;
	__istate_t istate;  
	if(OWI_reset()==OWI_RST_OK){
		istate = __get_interrupt_state();
		__disable_interrupt();	/* ATOMIC !!! */
		if (romcode) {		// Romcode oder Skip-Rom senden
			OWI_WriteByte(OWI_MATCH_ROM);
			for (i=0; i<8; i++) {
				OWI_WriteByte(*romcode++);
			}
		} else {
			OWI_WriteByte(OWI_SKIP_ROM);
		}
		/* COPY SCRATCHPAD senden und ggf. Strong Pull Up aktivieren. */
		OWI_WriteByte(OWI_RECALL_E2);
		if (pullstrong == OWI_PULL_STRONG) {
			OWI_pull_high();
		}
		__set_interrupt_state(istate); /* ATOMIC END */
	}
}

/************************************************************************************************
 * Das Scratchpad eines Busteilnehmers lesen lassen.
 * Parameter: 	romcode oder 0 für SkipRom (nur verwenden wenn nur ein Device angeschlossen ist!)
 * 		Zeiger auf das Zielarray für das scratchpad
 * Return:	0 = Erfolg; 1 = Fehler
 ***********************************************************************************************/
uint8_t DS1820_ReadScratch(uint8_t *romcode, uint8_t *scratchpad) 
{
	uint8_t i, crc=0;
	__istate_t istate;  

	if(OWI_reset()!=OWI_RST_OK) {return OWI_FAIL;}	// Reset prüfen und bei Fehler abbrechen

	istate = __get_interrupt_state();
	__disable_interrupt();				// ATOMIC
	if (romcode) {					// Romcode oder Skip-Rom senden
		OWI_WriteByte(OWI_MATCH_ROM);
		for (i=0; i<8; i++) {
			OWI_WriteByte(*romcode++);
		}
	} else {
		OWI_WriteByte(OWI_SKIP_ROM);
	}
	OWI_WriteByte(OWI_READ_SCRATCHPAD);
	for (i=0; i<9; i++){				// 9 Bytes lesen und CRC prüfen
		*(scratchpad++)=OWI_ReadByte();
		crc = OWICRC_lookup(crc, *scratchpad);
	}
	__set_interrupt_state(istate); 			// ATOMIC END

	if (crc) {return OWI_FAIL;} 			// Wenn crc > 0 war der crc Fehlerhaft
	return OWI_OK;					// sosnt ist alles ok
}

/************************************************************************************************
 * TH und TL Register im Scratchpad eines Busteilnehmers schreiben
 * Input:  romcode oder 0 für SkipRom (nur verwenden wenn nur ein Device angeschlossen ist!)
 * 	   Wert für TH und Wert für TL
 ***********************************************************************************************/
void DS18B20_WriteScratch(uint8_t *romcode, uint8_t TH, uint8_t TL, uint8_t config)
{
	uint8_t i;
	__istate_t istate;  
	if(OWI_reset()==OWI_RST_OK){
		istate = __get_interrupt_state();
		__disable_interrupt();	/* ATOMIC !!! */
		/* Romcode übertragen oder SKIP ROM senden */
		if (romcode) {
			OWI_WriteByte(OWI_MATCH_ROM);
			for (i=0; i<8; i++) {
				OWI_WriteByte(*romcode++);
			}
		} else {
			OWI_WriteByte(OWI_SKIP_ROM);
		}
		/* 2 oder 3 Bytes in das Scratchpad schreiben */
		OWI_WriteByte(OWI_WRITE_SCRATCHPAD);
		OWI_WriteByte(TH);
		OWI_WriteByte(TL);
		if (config) {OWI_WriteByte(TL);}
		__set_interrupt_state(istate); /* ATOMIC END */
	}
	OWI_reset();
}

/************************************************************************************************
 * Rohdaten in Temperatur mit wählbarer Auflösung umwandeln
 * Input: 	Zeiger auf das Scratchpad array, anzahl der dezimalstellen (max 4)
 * Return: 	Temperatur * 10(*Anzahl der dezimalstellen) als Ganzahl,
 * 		bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS18B20_CalcTemp (uint8_t *scratchpad, uint8_t decimals)
{
	uint8_t i, round;
	uint16_t result, frac;

	result = (uint16_t) scratchpad[1] << 8;
	result |= scratchpad[0];

	frac = result & 0x0F;
	frac *= 625;
	
	result = result >> 4;

	for (i=0; i<decimals; i++){	
		result *= 10;
	}
	for (i=4; i>decimals; i--){	
		round = frac%10;
		frac /=10;
		if (round >= 5){frac+=1;}
	}
	return result + frac;
}

/************************************************************************************************
 * Rohdaten in Temperatur mit 0,5° Auflösung umwandeln
 * Input: Zeiger auf das Scratchpad array
 * Return: Temperatur * 10 als Ganzahl, bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS1820_CalcLowRes (uint8_t *scratchpad) 
{
	uint16_t wert;
	wert = *(scratchpad+1) << 8;	/* wichtig, sonst MSbyte nach 2er Komplemnt falsch! */
	wert |= *scratchpad;
	if (*(scratchpad+1)) 	/* Bei negativer Temperatur 2er Komplement bilden */
		wert = ~wert +1;
	wert = wert >> 1;	/* 0.5° Bit entfernen (Muß hinter 2er Komplementbildung stehen) */	
	wert *= 10;		/* Festkommazahl */
	wert += ((*scratchpad) & 1) * 5 ;	/* Wenn 0.5° Bit gesetzt war, 5 hinzufügen */
	if (*(scratchpad+1))	/* Bei negativer Temperatu wieder 2er Komplement bilden */ 
		wert = ~wert +1;
	return wert;
}

/************************************************************************************************
 * Rohdaten in Temperatur mit 0,1° Auflösung umwandeln
 * Input: Zeiger auf das Scratchpad array
 * Return: Temperatur * 10 als Ganzahl, bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS1820_CalcHighRes (uint8_t *scratchpad) 
{
	uint16_t wert;
	wert = *(scratchpad+1) << 8;	/* wichtig, sonst MSbyte nach 2er Komplemnt falsch! */
	wert |= *scratchpad;
	wert = wert >> 1;	/* 0.5° Bit entfernen (Muß vor 2er Komplementbildung stehen!) */	
	/* Wenn negativer Wert (was auch bei Tempread=0 und CountRemain=12 zutrifft!!!) */
	if (*(scratchpad+1) || ((!*scratchpad)&&(*(scratchpad+6) >12))) {
		if (*(scratchpad+1)) wert = ~wert +1;
		wert *= 16;
		wert -= 12;
		wert += *(scratchpad+6);
	} else {
	/* Wenn positiver Wert */
	/* T=TR-0.25+(16-CR/16)=TR-0,25+1-CR/16=TR+0,75-CR/16 =>  16T=16TR+12-CR */
		wert *= 16;
		wert += 12;
		wert -= *(scratchpad+6);
	}
	/* => T=(16TR+12-CR)/16 => 10T=(16TR+12-CR)*10/16, gerundet 10T=((16TR+12-CR)*10+8)/16 */
	wert *= 10;	/* Festkommaberechnung */
	wert += 8;	/* Rundung */
	wert /= 16;
	if (*(scratchpad+1)) wert = ~wert +1;	/* Bei negativem Wert wieder 2er Komplement */
	return wert;
}

/************************************************************************************************
 * Temperatur String umwandeln
 * Input: Zeiger auf den Zielstring (min 6 byte), Temperatur * 10 als Ganzzahl, bei negativen
 * 			Werten als Zweierkomplement
 ***********************************************************************************************/
void DS1820_PrintTemp (char *buffer, uint16_t wert) 
{
	/* Jede Ziffer extrahieren und durch +48 in das richtige ASCII zeichen wandeln */
	*buffer = ' ';
	if (wert & (1 << 15)) {
		wert = ~wert +1;
		*buffer = '-';
	}
	if (wert > 999){	/* wenn Wert kleiner 100.0, dann führende 0 nicht schreiben */  
		*buffer = wert / 1000 + 48;
	}
	wert %= 1000;	
	*(buffer+1) = wert / 100 + 48;
	wert %= 100;
	*(buffer+2) = wert / 10 + 48;
	*(buffer+3) = ',';
	wert %= 10;
	*(buffer+4) = wert + 48;
	*(buffer+5) = '\0';
}
