/************************************************************************************************
 * 	Projekt:	Funktionen für DS18x20 1-Wire Thermometer
 * 	Autor:		Jan-Ole Spahn
 * 	Beschreibung:	Devicespezifische Routinen für 1-Wire Digitalthermometer
 * 	Version:	2.0, 02.12.2013 Gemeinsame Version für MSP430 und AVR, wählbar über
 *				        	 #define AVR oder #define __msp430 in owi.h
 * 			1.1, 16.11.2013 Portiert für MSP430, Unterstützung für DS18B20 und DS1822
 * 					hinzugefügt
 * 			1.0, 15.02.2012 Nur der DS18S20 wird momentan untesrtützt
 ************************************************************************************************/

#ifndef _DS1820_H
#define _DS1820_H

#include <stdint.h>

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Temperaturmessung durchführen lassem.
 * Parameter: romcode oder 0 für alle, 0 für kein oder 1 für Strong Pull-Up
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_StartConversion(uint8_t *romcode, uint8_t pullstrong);

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Register TH und TL ins EPROM schreiben lassen
 * Parameter: romcode oder 0 für alle
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_CopyScratchpad(uint8_t *romcode, uint8_t pullstrong);

/************************************************************************************************
 * Einen oder alle Busteilnehmer die Register TH und TL aus dem EPROM laden lassen
 * Parameter: romcode oder 0 für alle
 * 	      pullstrong 0: Strong Pull Up aktivieren, 1: nicht tun
 ***********************************************************************************************/
void DS1820_RecallEeprom(uint8_t *romcode, uint8_t pullstrong); 

/************************************************************************************************
 * Das Scratchpad eines Busteilnehmers lesen lassen.
 * Parameter: 	romcode oder 0 für SkipRom (nur verwenden wenn nur ein Device angeschlossen ist!)
 * 		Zeiger auf das Zielarray für das scratchpad
 * Return:	0 = Erfolg; 1 = Fehler
 ***********************************************************************************************/
uint8_t DS1820_ReadScratch(uint8_t *romcode, uint8_t *scratchpad);

/************************************************************************************************
 * TH, TL und config Register im Scratchpad eines Busteilnehmers schreiben
 * Input:  romcode oder 0 für SkipRom (nur verwenden wenn nur ein Device angeschlossen ist!)
 * 	   Werte für TH, TL und Config Register
 ***********************************************************************************************/
void DS18B20_WriteScratch(uint8_t *romcode, uint8_t TH, uint8_t TL, uint8_t config);

/************************************************************************************************
 * TH und TL Register im Scratchpad eines Busteilnehmers schreiben
 * Input:  romcode oder 0 für SkipRom (nur verwenden wenn nur ein Device angeschlossen ist!)
 * 	   Wert für TH und Wert für TL
 ***********************************************************************************************/
#define DS1820_WriteScratch(a,b,c) DS18B20_WriteScratch(a,b,c,0)

/************************************************************************************************
 * Rohdaten in Temperatur mit 0,5° Auflösung umwandeln
 * Input: Zeiger auf das Scratchpad array
 * Return: Temperatur * 10 als Ganzahl, bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS1820_CalcLowRes (uint8_t *scratchpad);

/************************************************************************************************
 * Rohdaten in Temperatur mit 0,1° Auflösung umwandeln
 * Input: Zeiger auf das Scratchpad array
 * Return: Temperatur * 10 als Ganzahl, bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS1820_CalcHighRes (uint8_t *scratchpad); 
#define DS18S20_CalcHighRes(a) DS1820_CalcHighRes(a)

/************************************************************************************************
 * Rohdaten in Temperatur mit 0,1° Auflösung umwandeln
 * Input: Zeiger auf das Scratchpad array, anzahl der Dezimalstellen (0-4)
 * Return: Temperatur als Ganzahl(*10^Dezimalstellen), bei negativen Werten als Zweierkomplement
 ***********************************************************************************************/
uint16_t DS18B20_CalcTemp (uint8_t *scratchpad, uint8_t decimals);

#define DS18B20_CalcHighRes(a) DS18B20_CalcTemp(a, 1)
#define DS1822_CalcHighRes(a) DS18B20_CalcTemp(a, 1)

/************************************************************************************************
 * Temperatur String umwandeln
 * Input: Zeiger auf den Zielstring (min 6 byte), Temperatur * 10 als Ganzzahl, bei negativen
 * 			Werten als Zweierkomplement
 ***********************************************************************************************/
void DS1820_PrintTemp (char *buffer, uint16_t wert);

#endif
