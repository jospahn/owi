/************************************************************************************************
 * 	Projekt:	Treiber für den Dallas 1-Wire Bus	
 * 	Autor:		Jan-Ole Spahn
 * 	Beschreibung:	Allgemeine Funktionen zur Ansteuerung des 1-Wire Bus von Dallas
 * 			Semiconductors / Maxim.
 * 			Bit und Byteweise Lese und Schreibfunktionen, sowie Typ unabhängige
 * 			Funktionen.
 *	Version:	2.0, 02.12.2013	-Gemeinsame Version für MSP430 und AVR, wählbar über
 *				         #define AVR oder #define __msp430 in owi.h
 * 			1.2, 16.11.2013 -Portiert auf MSP430, Rom Search Rouitne ersetzt, Makros 
 * 					 zur Portmanipulation verbessert
 * 			1.1, 15.02.2012	-Ansteuerung für Strong pull up hinzugefügt (PZ)
 *
 ************************************************************************************************/
#ifndef _OWI_H
#define _OWI_H

#include <stdint.h>
#include "../main.h" 		// we need to know the cpu frequnecy

/****************** Diverse Einstellparameter ****************************************************/
#define OWI_MAX_DEVICES	10	// Anzahl der OWI Slaves
#define OWI_PZ_AKTIV		// Pull Strong FET angeschlossen, sonst auskommentieren

//#define AVR			// AVR Hardware, sonst auskommentieren (in AVR gcc schon definiert?)
//#define __msp430		// MSP430 Hardware

/*** Port mapping ***/
/* Datenleitung DQ */
#if defined(__msp430)
  #define OWI_PORT_DQ		P11OUT
  #define OWI_DDR_DQ		P11DIR
  #define OWI_PIN_DQ		P11IN
  #define OWI_DQ		BIT0
  #define OWI_DQ_BIT_POS	0
  #define OWI_DS_DQ		P11DS
#elif defined(AVR)
  #define OWI_PORT_DQ		PORTD
  #define OWI_DDR_DQ		DDRD
  #define OWI_PIN_DQ		PIND
  #define OWI_DQ		0x80 //(1<<OWI_DQ_BIT_POS)
  #define OWI_DQ_BIT_POS	7
#endif

/* Ansteuerleitung für Pull Strong Mosfet (invertierend) */
#if defined(__msp430) && defined(OWI_PZ_AKTIV)
  #define OWI_PORT_PZ		P11OUT
  #define OWI_DDR_PZ		P11DIR
  #define OWI_PIN_PZ		P11IN
  #define OWI_PZ	 		BIT1
#elif defined(AVR) && defined(OWI_PZ_AKTIV)
  #define OWI_PORT_PZ		PORTB
  #define OWI_DDR_PZ		DDRD
  #define OWI_PIN_PZ		PIND
  #define OWI_PZ	 	64 //(1<<6)
#endif

/*** Makrodefinitionen für Funktionsparameter etc. */
#define	OWI_PULL_STRONG		0
#define	OWI_PULL_STRONG_NOT	1

#define OWI_SEARCH_ALL		0
#define OWI_SEARCH_ALARM	1

#define OWI_RST_OK			0
#define OWI_RST_FAIL		1
#define OWI_RST_SHORT		2

#define OWI_ROM_FAIL		0

#define OWI_FAIL		1
#define OWI_OK			0

/*** Wartezeiten für das Bustiming ***/
#ifdef AVR
  #define MHZ	1
#else
  #define MHZ	(F_MCLK/1000000)
#endif

#define OWI_delay_A (6 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_B (64	* MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_C (60	* MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_D (10	* MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_E (9 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_F (55	* MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_G (0 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_H (480 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_I (80	* MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_J (400 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
//#define OWI_delay_K (0 * MHZ)	// Definiert Wartezeit für Timing der Bitfunktionen
#define OWI_delay_L (40 * MHZ)	// extra hinzugefügt, benötigt in rom-search

/*** One Wire Function Command Set ***/
/* ROM Commands */
#define OWI_SEARCH_ROM		0xF0
#define OWI_READ_ROM		0x33	// ROM code eines einzelenen Teilnehmers lesen
#define OWI_MATCH_ROM		0x55	// Einzelnen Busteilnehmer adressieren
#define OWI_SKIP_ROM		0xCC	// Alle adressieren (macht nur bei einem Sinn)
#define OWI_ALARM_SEARCH	0xEC	// Wie search rom, aber nur für devices im Alarmzustand
/* DS 18S20 */
#define OWI_CONVERT_T		0x44	// Initiates temprature conversion
#define OWI_READ_SCRATCHPAD	0xBE	// Reads the entire scratchpad including the CRC byte
#define OWI_WRITE_SCRATCHPAD	0x4E	// Writes Data into scratchpad bytes 2 and 3 (Th and Tl) 
#define OWI_COPY_SCRACTHPAD	0x48	// Copies Th and Tl data from scratchpad to EEPROM
#define OWI_RECALL_E2		0xB8	// Recalls Th and Tl data from EEPROM to scratchpad
#define OWI_READ_POWERSUPPLY	0xB4	// Signals DS18S20 power supply mode to the master

/*** Family Codes ***/
#define OWI_DEV_18S20		0x10	// high precision digital thermometer
#define OWI_DEV_1822		0x22	// econo digital thermometer
#define OWI_DEV_18B20		0x28	// programmable resolution digital thermometer

/*** Makros ***/
/* Port register manipulieren */
#define OWI_low()         OWI_PORT_DQ&=~OWI_DQ		
#define OWI_in()          OWI_DDR_DQ&=~OWI_DQ
#define OWI_out()         OWI_DDR_DQ|=OWI_DQ	

#ifdef  OWI_PZ_AKTIV	
  #define OWI_PZ_out() 	  OWI_DDR_PZ|=OWI_PZ		
  #define OWI_PZ_low()	  OWI_PORT_PZ&=~OWI_PZ 	
  #define OWI_PZ_high()   OWI_PORT_PZ|=OWI_PZ
  #define OWI_pull_low()  OWI_PZ_high(); OWI_out() 		
  #define OWI_release()	  OWI_PZ_high(); OWI_in() 		
  #define OWI_pull_high() OWI_in(); OWI_PZ_low()
#else				
  #define OWI_high()      OWI_PORT_DQ|=OWI_DQ		
  #define OWI_pull_low()  OWI_low(); OWI_out() 		
  #define OWI_release()	  OWI_low(); OWI_in() 		
  #define OWI_pull_high() OWI_high(); OWI_out()
  #ifdef __msp430
    #define OWI_full_DS()	  OWI_DS_DQ|=OWI_DQ
  #endif
#endif


/*** Kompatibilität der Atomic Funktionen ***/
#ifdef AVR
#define __delay_cycles(a) _delay_us(a)
#define __istate_t unsigned char
#define __get_interrupt_state()	((1<<7)&SREG)
#define __disable_interrupt() cli()
#define __set_interrupt_state(a) SREG|=a
#endif


/*** Prototypen ***/

/************************************************************************************************
 * Den OWI port konfigurieren
 ***********************************************************************************************/
void OWI_init (void);	

/************************************************************************************************
 * Reset Puls auf den Bus geben, und Presence Puls abwarten.
 * return 0: ok, return 1: keine Antwort erhalten, return 2: Kurzschluss 
 ***********************************************************************************************/
uint8_t OWI_reset (void);

/************************************************************************************************
 * Byte vom Bus lesen, return: gelesenes Byte
 ***********************************************************************************************/
uint8_t OWI_ReadByte (void);

/************************************************************************************************
 * Byte auf den Bus schreiben
 ***********************************************************************************************/
void OWI_WriteByte(uint8_t data);

/*************************************************************************************************
 * Liest den Rom Code eines angeschlossenen 1-Wire Devices.
 * Es darf nur 1 Device angeschlossen sein!
 * In: Zeiger auf uint8_t array[8] 
 * Return: 0 wenn CRC erfolgreich
 ************************************************************************************************/
uint8_t OWI_ReadRom(uint8_t *romcode);

/*************************************************************************************************
 * Liest den Rom Code von n angeschlossenen 1-Wire Devices.
 * Parameter: 	2 Dimensionales Array vom Typ uint8_t, in diesem werden die Romcodes
 * 		gespeichert. Zwiete Diemsion muß die Größe 8 haben, da ein Romcode 8 Byte
 * 		lang ist.
 * 		max: Anzahl der maximal zu findenen Romcodes 
 * 		alarm: wenn 1 alarm search statt search rom durchführen
 * Return: 	0 = Fehler oder n = Anzahl der gefunden Romcodes 
 ************************************************************************************************/
uint8_t OWI_Search(uint8_t *pRomcode, uint8_t max, uint8_t alarm);

/************************************************************************************************
 * Einen oder alle Busteilnehmer nach ihrem Powermode fragen.
 * Parameter: romcode oder 0 für alle
 * return 1: einer oder meherere mit Parasitepower, return 0: Kein Parasitepower
 ***********************************************************************************************/
uint8_t OWI_ReadPowerMode (void);

#endif
