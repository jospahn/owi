/************************************************************************************************
 * 	Projekt:	Treiber für den Dallas 1-Wire Bus	
 * 	Autor:		Jan-Ole Spahn
 * 	Beschreibung:	Allgemeine Funktionen zur Ansteuerung des 1-Wire Bus von Dallas
 * 			Semiconductors / Maxim.
 * 			Bit und Byteweise Lese und Schreibfunktionen, sowie Typ unabhängige
 * 			Funktionen.
 *	Version:	2.0, 02.12.2013	-Gemeinsame Version für MSP430 und AVR, wählbar über
 *				         #define AVR oder #define __msp430 in owi.h
 * 			1.2, 16.11.2013 -Portiert auf MSP430, Rom Search Rouitne ersetzt, Makros 
 * 					 zur Portmanipulation verbessert
 * 			1.1, 15.02.2012	-Ansteuerung für Strong pull up hinzugefügt (PZ)
 *
 ************************************************************************************************/

/******* Includes ******************************************************************************/
#ifdef AVR
  #include <avr/interrupt.h>
  #include <avr/delay.h>
#endif
#include <stdlib.h>
#include <stdint.h>
#include "owi.h"
#include "owicrc.h"

/******* Prototypen ****************************************************************************/
void OWI_Write1 (void);
void OWI_Write0 (void);
void OWI_WriteBit (uint8_t bit);
uint8_t OWI_ReadBit (void);


/******* Generelle Bus-Manipulation (lesen, schreiben, reset, Kurzschluss erkennen) ************/

/************************************************************************************************
 * Den OWI port konfigurieren
 ***********************************************************************************************/
void OWI_init (void)				
{
	OWI_in();
	OWI_low();
	#if defined(OWI_PZ_AKTIV)
		OWI_PZ_high();
		OWI_PZ_out();
	#elif defined(__msp430)
		OWI_full_DS();
	#endif
}

/************************************************************************************************
 * Ein Bit auf den Bus schreiben
 ***********************************************************************************************/
void OWI_Write1 (void)
{
	OWI_pull_low();
	__delay_cycles(OWI_delay_A);
	OWI_release();
	__delay_cycles(OWI_delay_B);
}

void OWI_Write0 (void)
{
	OWI_pull_low();
	__delay_cycles(OWI_delay_C);
	OWI_release();
	__delay_cycles(OWI_delay_D);
}

/************************************************************************************************
 * Bit vom Bus lesen, return: gelesenes Bit
 ***********************************************************************************************/
uint8_t OWI_ReadBit (void)
{
	OWI_pull_low();			// Bus für 6µs auf low ziehen
	__delay_cycles(OWI_delay_A);
	OWI_release();	
	__delay_cycles(OWI_delay_E);	// und nach weiteren 10µs Bit lesen
	return (OWI_PIN_DQ & OWI_DQ) >> OWI_DQ_BIT_POS;
}

/************************************************************************************************
 * Byte vom Bus lesen, return: gelesenes Byte
 ***********************************************************************************************/
uint8_t OWI_ReadByte (void)
{
    uint8_t data;
    uint8_t i;
 
    data = 0x00;
    /* 8 Bits einzeln lesen */
    for (i = 0; i < 8; i++)
    {
        data >>= 1;	/* rechts schieben */
	/* MSB setzten, wenn eine 1 gelesen wurde */
        if (OWI_ReadBit())
        {
            data |= 0x80;
        }
	/* 55us warten (bis Bit zu Ende ist ) */
	__delay_cycles(OWI_delay_F);
    }
    return data;
}

/************************************************************************************************
 * Byte auf den Bus schreiben
 ***********************************************************************************************/
void OWI_WriteByte(uint8_t data)
{
    uint8_t bitmask = 1;
    do{
	if (data & bitmask) {
		OWI_Write1();
	} else {
		OWI_Write0();
	}
    }while(bitmask <<= 1);
}

/************************************************************************************************
 * Reset Puls auf den Bus geben, und Presence Puls abwarten.
 * return 0: ok, return 1: keine Antwort erhalten, return 2: Kurzschluss 
 ***********************************************************************************************/
uint8_t OWI_reset (void) 
{
	uint8_t presence ;	
	__istate_t istate = __get_interrupt_state();
	__disable_interrupt();	/* ATOMIC !!! */
	OWI_pull_low();				//Bus initialisieren (480µs low ziehen)
	__delay_cycles(OWI_delay_H);	 			
	OWI_release();			
	__delay_cycles(OWI_delay_I);		//nach weitern 80µs Antwort prüfen
	presence = (OWI_PIN_DQ & OWI_DQ) >> OWI_DQ_BIT_POS; //OWI_RST_FAIL
	if (!presence){				
		__delay_cycles(OWI_delay_J);	//weitere 400µs warten und auf Kurzschluss prüfen		
		presence = ((~OWI_PIN_DQ & OWI_DQ) >> OWI_DQ_BIT_POS) << 1;
	}
	__set_interrupt_state(istate); 		//ATOMIC END
	return presence;
}

/******* 1-Wire ROM Funktionen ******************************************************************/

/*************************************************************************************************
 * Liest den Rom Code eines angeschlossenen 1-Wire Devices (nur für 1 device stand alone)
 * Parameter: 	Zeiger auf ein Romcode Array (uint8_t [8])
 * Return: 	0 = Fehler, 1 = 1 Romcode gelesen
 ************************************************************************************************/
uint8_t OWI_ReadRom(uint8_t *romcode)
{
	uint8_t i, crc=0;
	__istate_t istate; 

	if (OWI_reset()!= OWI_RST_OK) {	return OWI_ROM_FAIL;} 
	istate = __get_interrupt_state();
	__disable_interrupt();				// ATOMIC 
	OWI_WriteByte(OWI_READ_ROM);	
	for (i=0; i<8; i++) {				//8 Bytes Romcode lesen und prüfen 	
		*(romcode++) = OWI_ReadByte();	
		crc = OWICRC_lookup(crc, *romcode);
	}
	__set_interrupt_state(istate); 			// ATOMIC END 
	if (crc) {return OWI_ROM_FAIL;}			// wenn CRC nicht 0 ist = fehler
	return 1;					// 1 Romcode gelesen
}

/*************************************************************************************************
 * Liest den Rom Code von max angeschlossenen 1-Wire Devices.
 * Parameter: 	Zeiger auf ein Romcode Array in dem die gefundenen Codes gespeichert werden.
 * 		max: Anzahl der maximal zu findenen Romcodes (OWI_MAX_DEVICES) 
 * 		alarm:  OWI_SEARCH_ALL oder OWI_SEARCH_ALARM
 * Return: 	0 = Fehler oder n = Anzahl der gefunden Romcodes 
 ************************************************************************************************/
uint8_t OWI_Search(uint8_t *pRomcode, uint8_t max, uint8_t alarm)
{
	uint8_t RomCnt=0, ByteCnt, BitCnt;	/* Schleifenzähler */
	uint8_t bitmask = 1;				
	uint8_t crc;
	uint8_t bits = 0;			
	uint8_t LastDiscrepancy = 0, LastZero;	
	__istate_t istate;

	do {//((RomCnt<=max) && (!crc) && (LastDiscrepancy))	
		if (OWI_reset()!=OWI_RST_OK) {return 0;}//Reset senden, Ende wenn keine Antwort 
		crc=0;
		LastZero = 0;
		ByteCnt = 0;
		BitCnt = 0;
			
		/* ROM Search oder ALARM Search senden */
		istate = __get_interrupt_state();
		__disable_interrupt();	
		if (alarm == OWI_SEARCH_ALARM) 
			{OWI_WriteByte(OWI_ALARM_SEARCH);}
		else	{OWI_WriteByte(OWI_SEARCH_ROM);}
		
		do {//(ByteCnt<8)	
			do {//(bitmask)
				/* Bit und Komplementbit lesen (AND aller SLAVES)*/
				bits = (OWI_ReadBit() << 1);
				__delay_cycles(OWI_delay_L);			
				bits |= OWI_ReadBit();
				__set_interrupt_state(istate); 
				__delay_cycles(OWI_delay_L);			
				if (bits == 3) {return 0;}	// 0b11 = keine Antwort->ENDE  
				if (bits == 0) {		// 0b00 = Diskrepanz
					if (BitCnt < LastDiscrepancy) {
						bits = *(pRomcode+((RomCnt-1)*8+ByteCnt)) & bitmask;
					}
					else if (BitCnt == LastDiscrepancy) {
						bits = 1;
					}
					else  {//(BitCnt > LastDiscrepancy)
						bits = 0;
					}
					if (bits == 0) {LastZero = BitCnt;}
				} else {		// 0b10 = eindeutig 1; 0b01 = eindeutig 0 
					bits >>= 1;
				}
				
				istate = __get_interrupt_state();
				__disable_interrupt();	
				if (bits) {
					*(pRomcode+(RomCnt * 8 + ByteCnt)) |= 0xFF & bitmask;
					OWI_Write1();
				} else {
					*(pRomcode+(RomCnt * 8 + ByteCnt)) &= 0xFF ^ bitmask;
					OWI_Write0();
				}
				BitCnt++;
				bitmask <<= 1;
			} while (bitmask);
			bitmask = 1;
			crc = OWICRC_lookup(crc, *(pRomcode+(RomCnt * 8 + ByteCnt)));
			ByteCnt++;
		} while (ByteCnt<8);
		__set_interrupt_state(istate); 
		LastDiscrepancy = LastZero;
		RomCnt++;
	} while ((RomCnt<=max) && (!crc) && (LastDiscrepancy));
	if (crc) RomCnt--;	
	return RomCnt;
}

/************************************************************************************************
 * Einen oder alle Busteilnehmer nach ihrem Powermode fragen.
 * Parameter: romcode oder 0 für alle
 * return 0: einer oder meherere mit Parasitepower, return 1: Kein Parasitepower
 ***********************************************************************************************/
uint8_t OWI_ReadPowerMode (void) 
{
	uint8_t mode;
	__istate_t istate;
	if (OWI_reset()!= OWI_RST_OK) {return 1;} 
	istate = __get_interrupt_state();
	__disable_interrupt();	/* ATOMIC !!! */
	OWI_WriteByte(OWI_SKIP_ROM);
	OWI_WriteByte(OWI_READ_POWERSUPPLY);
	mode = OWI_ReadBit();
	__set_interrupt_state(istate); /* ATOMIC END */
	return mode;
}
